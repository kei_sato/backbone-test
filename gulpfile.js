var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();

var paths = {
	'bower': './bower_components',
	'assets': './assets'
}

gulp.task('serve', function(){
	browserSync.init({
		server: {
			baseDir: './public'
		}
	});

	gulp.watch(paths.assets + '/styles/**/*.scss',['styles']);
	 gulp.watch(paths.assets + '/scripts/*.js',['scripts']);
	gulp.watch(paths.assets + '/pages/*' , ['pages']);

 	gulp.watch([paths.assets + '/styles/app.scss', 
 				paths.assets + '/pages/*', 
 				paths.assets + '/scripts/*'])
 			.on('change', browserSync.reload);
});

gulp.task('pages', function(){
	console.log('pages');
	return gulp.src([paths.assets + '/pages/*']).pipe(gulp.dest('./public'));

});

gulp.task('styles', function(){
	return gulp.src([
		paths.assets + '/styles/app.scss'
	])
	.pipe(sass({
		includePaths: [
			paths.bower + '/foundation/scss'
		]
	}))
	.pipe(concat('app.css'))
	.pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function(){
	gulp.src([
		paths.bower + '/jquery/dist/jquery.js',
		paths.bower + '/foundation/js/foundation.js',
		paths.bower + '/underscore/underscore-min.js',
		paths.bower + '/backbone/backbone-min.js',
		paths.bower + '/backbone-localstorage/backbone-localstorage.js'
	])
	.pipe(concat('base.js'))
	.pipe(gulp.dest('./public/js'));

	gulp.src([
		paths.assets + '/scripts/main.js'])
	.pipe(concat('main.js'))
	.pipe(gulp.dest('./public/js'));

});



gulp.task('default', ['serve']);



