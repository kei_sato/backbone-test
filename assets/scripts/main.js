
Sandwich = Backbone.Model.extend ({
	defaults : {
		name : '',
		bread : '',
		meat: '',
		toppings: '',
		rating : ''
	}
});


NewSandwichForm = Backbone.View.extend({
	el : $("#new-sandwich"),
	elForm : $('#sandwich-form'),
	renderedForm : '#rendered-form',

	events: {
		"click #add": "addForm",
		"click #submit" : "submitSandwich",
	},

	initialize: function(collection){
		this.sandwiches = collection;
	},

	addForm: function(e){
		e.preventDefault();

		var newform = $(this.elForm).html();
		$(this.el).append(newform);

	},

	submitSandwich: function(e){
		e.preventDefault();

		var _name = $('#name').val(),
			_bread = $('#bread').val(),
			_rating = $('#rating').val(),
			_meat = $('#meat').val(),
			_toppings = $('#toppings').val();



		var enteredSandwich = new Sandwich({name: _name, bread: _bread, meat: _meat, toppings: _toppings, rating: _rating});

		this.sandwiches.add(enteredSandwich);
		this.cleanView();
	},

	cleanView: function(){
		$(this.renderedForm).remove();
	}
});


SandwichesView = Backbone.View.extend({
	el : $("#sandwich"),
	currList : $("#sandwich-list"),
	sandTempl : $('#sandwich-template'),

	events: {
	},

	initialize: function(sandwiches){
		this.collection = sandwiches;
		this.collection.bind('add', this.render, this);

	},

    render: function(){
    	this.cleanView();

    	var template = $(this.sandTempl).html();
    	
    	var renderedTempl = _.template(template);
 	
   		renderedTempl = renderedTempl({list: this.collection.toJSON()});

    	$('#sandwich-list').append(renderedTempl);

    },

    cleanView: function(){
    	this.currList.children().remove();
    }
});



SandwichList = Backbone.Collection.extend ({
	model : Sandwich,
	view : SandwichesView,

	initialize: function () {
	}
});



var collection = new SandwichList();

var collectionView = new SandwichesView(collection);

var formView = new NewSandwichForm(collection);






